FROM golang:alpine as builder

WORKDIR /app

COPY ../hello_serv.go . 

RUN go mod init serv

RUN go mod download serv

RUN go build -o /app/build


FROM scratch

WORKDIR /build

COPY --from=builder /app/build /build

ENTRYPOINT [ "./build" ]

# FROM gcc:latest as builder

# COPY ./cpp_proj /usr/cpp/myapp

# WORKDIR /usr/cpp/myapp

# RUN gcc hello.cpp -o /usr/cpp/myapp/build

# RUN exec ./build

# FROM scratch

# WORKDIR /

# COPY --from=builder /usr/bin /usr/bin

# COPY --from=builder /usr/sbin /usr/sbin

# COPY --from=builder /usr/include /usr/include

# COPY --from=builder /usr/cpp/myapp/build /usr/cpp/myapp/build
# #usr/local/app

# ENTRYPOINT [ "usr/cpp/myapp/build" ]



